/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';

import AddItem from './components/molecules/AddItem';
import List from './components/molecules/List';

const App = () => {
  const [textItem, setTextItem] = useState('');
  const [itemList, setItemList] = useState([]);

  const onHandlerChangeItem = t => setTextItem(t);

  const onHandlerDelete = id => {
    setItemList(currentItems => currentItems.filter(item => item.id !== id));
  };

  const addItem = () => {
    setItemList(currentItems => [
      ...currentItems,
      {id: Math.random().toString(), value: textItem},
    ]);

    setTextItem('');
  };

  return (
    <View style={styles.container}>
      <AddItem
        onChangeText={onHandlerChangeItem}
        onPress={addItem}
        value={textItem}
      />
      <View style={styles.itemListContainer}>
        <List itemList={itemList} onPress={onHandlerDelete} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  itemListContainer: {
    flex: 1,
    marginTop: 15,
    paddingHorizontal: 10,
  },
});

export default App;
