import {Button, StyleSheet, TextInput, View} from 'react-native';

import React from 'react';

const AddItem = props => {
  const {onPress, onChangeText, value} = props;

  return (
    <View style={styles.inputContainer}>
      <TextInput
        placeholder="New item"
        onChangeText={onChangeText}
        style={styles.input}
        value={value}
      />
      <Button
        title="Add"
        color="#9191E9"
        onPress={onPress}
        disabled={value.trim().length === 0}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  input: {
    flex: 0.8,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
});

export default AddItem;
