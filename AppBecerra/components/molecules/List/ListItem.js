import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import React from 'react';

const ListItem = props => {
  const {item, onPress} = props;

  return (
    <View style={styles.container}>
      <Text style={styles.itemText} key={item.id}>
        {item.value}
      </Text>
      <TouchableOpacity
        style={styles.deleteContainer}
        onPress={onPress && onPress.bind(this, item.id)}>
        <Text style={styles.deleteText}>X</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    fontSize: 24,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  itemText: {
    color: '#000',
    flex: 0.9,
  },
  deleteContainer: {
    backgroundColor: '#FF0000',
    borderRadius: 50,
    width: 19,
    justifyContent: 'center',
  },
  deleteText: {
    color: '#FFF',
    textAlign: 'center',
  },
});
export default ListItem;
