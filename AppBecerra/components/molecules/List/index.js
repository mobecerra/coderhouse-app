import {FlatList, StyleSheet, Text, View} from 'react-native';

import ListItem from './ListItem';
import React from 'react';

const List = props => {
  const {itemList, onPress} = props;

  const _itemSeparator = () => {
    return <View style={styles.separator} />;
  };

  return (
    <FlatList
      data={itemList}
      nestedScrollEnabled
      renderItem={data => <ListItem onPress={onPress} item={data.item} />}
      keyExtractor={item => item.id}
      contentContainerStyle={{paddingBottom: 15}}
      ItemSeparatorComponent={_itemSeparator}
      ListEmptyComponent={() => <Text>The list is empty</Text>}
      ListHeaderComponent={() => <Text style={styles.title}>Item List</Text>}
    />
  );
};

const styles = StyleSheet.create({
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#CED0CE',
  },
  title: {
    fontSize: 17,
    marginTop: 20,
    marginBottom: 15,
    fontWeight: 'bold',
    color: '#000',
  },
});

export default List;
